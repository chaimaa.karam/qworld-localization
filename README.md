# QWorld Localization


## Info

This repository manages the translations into different languages of the QWorld tutorials:

- [bronze-qiskit](https://gitlab.com/qworld/bronze-qiskit)
  - Bengali ([bn](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/bn))
  - Chinese, Simplified ([zh_CN](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/zh_CN))
  - Chinese, Traditional ([zh_TW](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/zh_TW))
  - Czech ([cs](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/cs))
  - Dutch ([nl](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/nl))
  - Filipino ([fil](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/fil))
  - French ([fr](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/fr))
  - German ([de](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/de))
  - Persian ([fa](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/fa))
  - Polish ([pl](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/pl))
  - Spanish ([es](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/es))

- [nickel](https://gitlab.com/qworld/nickel)
  - Spanish ([es](https://gitlab.com/clausia/qworld-localization/-/tree/main/nickel/es))

- [silver](https://gitlab.com/qworld/silver)


## Rules for the Womanium Project

All your work should be done in this repository.

Use the proper locale folder (listed above).

**Steps**
1. Create an issue to indicate that a particular file is going to be translated
   - example:
     - https://gitlab.com/clausia/qworld-localization/-/issues/4
2. When you have the translation, create a Merge Request for the modified file _(a Merge request (or MR) is the equivalent of a GitHub Pull Request (PR))_
3. In an existing MR, indicate that this include the complete translation, ideally another person will come and indicate that they will proofread the file in said MR, but if you are the only one who is translating, then you will have to do the two steps yourself
   - you must leave a comment to indicate that you are working on the proofread
   - the comments that arise from the proofread are left in the same MR, if it is possible, indicate your comment in the relevant line of code
     - example of comments on a specific line, see MR: https://gitlab.com/clausia/qworld-localization/-/merge_requests/3 (just as an example)
4. The entire context is versioned, in each folder by language, that is, the folder structure, images, extra files, in this way you can: either clone the repo, or update the corresponding locale folder constantly in your local computer. The ideal is to have a local environment so that you can check the proper functioning of the notebook before doing the MR
5. It is not worth doing a half translation, that is, if you make an MR with an incomplete translation, it will be marked with a flag, if you want to start an MR with an incomplete translation, the MR must be in _Draft_, otherwise it will be marked
6. Once an MR has indicated that it has been proofread, then it can be _merged_
7. The work of a completed and _merged_ MR will be counted for both the translator and the proofreader (ideally, the proofreader should be someone other than the translator)

**Notes**
- The notebook file name must stay the same, it is not translated, and nothing is added to its name.
- Do not leave the outputs of the code cells if the original notebook does not have them, it is true that you have to validate the operation, but once that is done, you must eliminate the output (if applicable)

### Criteria to consider for your Womanium project

- Translation quality
- Proofreading quality
- Number of notebooks translated
- Number of proofread notebooks
- Follow-up of the established rules
- Amount of typos found in your translations
